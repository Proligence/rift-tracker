﻿using System.Web;
using System.Web.Mvc;

namespace Proligence.Rift.Tracker.Client
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}