﻿using System;
using System.Threading;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Hosting;
using Owin;
using RiftDotNet;
using SharpDX;

namespace Proligence.Rift.Tracker
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            // This will *ONLY* bind to localhost, if you want to bind to all addresses
            // use http://*:8080 to bind to all addresses. 
            // See http://msdn.microsoft.com/en-us/library/system.net.httplistener.aspx 
            // for more information.
            string url = "http://localhost:9021";
            using (WebApp.Start(url))
            {
                Console.WriteLine("Started Oculus Rift tracker server on: {0}", url);
                var hub = GlobalHost.ConnectionManager.GetHubContext<SenderHub>();
                using (var mgr = new HMDManager())
                {
                    // We need to ensure that the user has attached his rift to the computer
                    // or wait until he has done so.
                    var hmd = mgr.AttachedDevice;
                    if (hmd == null)
                    {
                        hub.Clients.All.addMessage("Please attach your rift to the computer...");
                        Console.WriteLine("Please attach your rift to the computer...");
                        hmd = mgr.WaitForAttachedDevice(null);
                    }

                    hub.Clients.All.addMessage(string.Format("Found HMD at: {0}", hmd.Info.DisplayDevice));
                    Console.WriteLine("Found HMD at: {0}", hmd.Info.DisplayDevice);
                    hub.Clients.All.addMessage(string.Format("Manufacturer: {0}", hmd.Info.Manufacturer));
                    Console.WriteLine("Manufacturer: {0}", hmd.Info.Manufacturer);

                    Console.WriteLine("Data feed started...");
                    while (true)
                    {
                        hub.Clients.All.setAcceleration(hmd.Acceleration.X, hmd.Acceleration.Y, hmd.Acceleration.Z);
                        hub.Clients.All.setAngularVelocity(hmd.AngularVelocity.X, hmd.AngularVelocity.Y, hmd.AngularVelocity.Z);

                        Thread.Sleep(20);

                        if (Console.KeyAvailable)
                        {
                            var key = Console.ReadKey(false);
                            if (key.Key == ConsoleKey.C && key.Modifiers.HasFlag(ConsoleModifiers.Control))
                            {
                                Console.WriteLine("Stopping server...");
                                Thread.Sleep(3000);
                                break;
                            }
                        }
                    }

                    hmd.Reset();
                }
            }
        }
    }

    class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);
            app.MapSignalR();
        }
    }
    public class SenderHub : Hub
    {
    }
}
