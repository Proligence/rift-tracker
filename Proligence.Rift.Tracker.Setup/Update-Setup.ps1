param ([switch]$CommitChanges, [switch]$NewWix)

$wd = Get-Location
Write-Host "Working directory: " $wd

# Build release
C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe /t:Rebuild /p:Configuration=Release ..\Proligence.Rift.Tracker.sln

# Remove XML doc files
Remove-Item '..\Proligence.Rift.Tracker\bin\Release\*.xml' -Force -Verbose

# Update wix components
$WixCompGenPath = Join-Path $wd '..\lib\wix\WixCompGen.exe'
$cmd = $WixCompGenPath + ' ' + `
    '/FragmentPropertyName:RiftTrackerComponents ' + `
    '/ContainerDirectoryId:dir_RiftTracker ' + `
    '/SourceRoot:"`$(var.SolutionDir)Proligence.Rift.Tracker\bin\Release" ' +
    '/ComponentGroupId:cg_RiftTrackerComponents ' + `
    '/SourcePath:"' + $wd + '..\..\Proligence.Rift.Tracker\bin\Release" ' + `
    '/Output:"' + $wd + '"\RiftTrackerComponents.wxs ' + `
    '/IdPrefix:rtc_ '
if (-not $NewWix) {
	$cmd = $cmd + '/Update '
}
Write-Host $cmd
Invoke-Expression $cmd